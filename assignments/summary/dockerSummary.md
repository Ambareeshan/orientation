### **DOCKER** 

Docker is an open platform for developing, shipping and running applications.To manage the _dependencies_ as we buid large projects and deploy them in various environments, we use docker. A **docker** is a program for developers to develop, and run applications with containers.It is a tool designed to create, develop and run applications using containers. The containers have all the essentials such as libraries and dependencies as a single package to run the application.

A **docker engine** is a _Client-Server_ application that has the following components:  
  1. A server  
  2. A REST API  
  3. Command Line Interface.

![DOCKER][IMAGE2]  

[IMAGE2]: https://docs.docker.com/engine/images/engine-components-flow.png

> The Command Line Interface uses the REST API to control or interact with the docker daemon. The docker daemon is a service that runs on the host machine. The daemon creates and manages Docker _objects_, like _images, containers, networks and volumes_.

The docker architecture is a client-server architectue. The docker client talks to the daemon which builds, runs and manages the Docker containers, using a REST API over a UNIX sockets or network interface.

 # _Docker image_
* A Docker image contains everything needed to run an application as a container.A docker image is a read only template that has a set of instructions required to create a docker container which can run on docker platform. The docker images contain a collection of files that are have all the essentials such as installattions, application code, and dependencies required to configure a fully functional container environment.  
This includes:    
   * code
   * run time
   * libraries
   * environment variables
   * configuration files
* The images can be deployed in any Docker environment as a container.

**_Container_** :It is a running Docker image. We can create multiple containers from one image. Containers are great for continuous integration and continuous delivery (CI/CD) workflows.According to Docker, the container is simple, stand-alone, capable of running a piece of software piece that covers everything needed to run it.  
**_Docker hub_**:It is like GitHub or docker images and container.Docker's official image can be accessed for more than 100,000 container images supplied by software vendor, open source projects.

## **Some Basic Commands:**
_docker ps_: Alllows us to view all the containers that are running on the Docker Host.  
$docker ps 

_docker start_: For starting any stopped containers.    
 $docker start  

_docker stop_: Stops any running container.   
$docker stop    

_docker run_: Creates container from docker images.  
$docker run   

_docker rm_: deletes the containers    
docker rm    

**Some common operations include:**  
1. Download images 
2. Copy code
3. Access docker terminal
4. Install dependencies
5. Compile and run code 
6. Document steps to run the program in README.md file
7. Commit changes done
8. Push docker image to docker-hub
   