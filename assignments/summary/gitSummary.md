### **GIT summary**
 
Git is a commony used source code repository. It is a  version-control software that makes collaboration with teammates super simple. It is a version control system used to track the changes in the files, and also work on those files among multiple people. It helps in keeping track of the changes made. It helps in synchronising the code between people. Different members of a team might make different changes on a file. The git takes all those independent changes and merges them in to the **MASTER** .This ensure that everyone works on the recent version of the repository.  

![git working][IMAGE]

[IMAGE]: https://miro.medium.com/max/686/1*diRLm1S5hkVoh5qeArND0Q.png

## **_Required Vocabulary_**
# _Repository:_
* It is often called repo. A repository is a collection of files or folders that may contain the code which we use git to track.  

# _Gitlab:_
* It is a popular remote storage solution or git repos..  

# _Commit:_  
* This is like saving the work we do. When we commit to a repository, we save it as they exist at that moment. The commit will onlly exist on the local machine until it is pushed to a remote repository.  

# _Push:_
* Pushing is essentially syncing the commits we make to Gitlabs. Push is used when we move a file from a local repository to a remote repository. Pushing is how one transfers the commits from the local repository to a remote repository.  

# _Branch:_  
* If we see the repository as a tree, the Master Branch is the main software which will be the trunk of the tree. The branches of this tree is called the branches which will contain instances of code that is different from the main codebase.  

# _Merge:_  
* When the branch created is free of  bugs, and ready to become a part of the primary code base, it will get merged into master branch.  

# _Clone:_  
* Cloning takes an enitre online repository and makes an exact copy of it on the local machine.  

# _Fork:_  
* Forking is like cloning. Instead of copying the entire repo, we get a new repo in our name.  

# _Add:_  
* The git add command is the first command in a series of commands that follows. Basically, using the add command moves the file to the staging area, from where we can commit the file to the repository using the _git commit_ command.  



There are four four fundamental elements in the GIT workflow  
  * Working Directory  
  * Staging Area  
  * Local Repository  
  * Remote Repository  
If we consider the Working Directory, a file in the Working directory can be in three stages:  
 1. **It can be staged**: This means that a file which has been updated with the changes has been marked to be commited to the local repository but has not yet been commited.  
 2. **It can be modified**: This means that the file with updated changes has not yet been stored in the local repository.  
 3. **It can be commited**:This means that the file with the updated changes has been safely stored in the local repository.  

## **Some basic commands**
1. _git clone_ - to clone the repo
2. _git commit_ - to commit to the repo. All the files that are staged will be added to the local repository.
3. _git push_ - the commands is used to push the files in a local repository to a remote repository. Anyone with the access to the remote repository will be able to watch the changes.  
4. _git checkout -b<branch name>_  - to create a branch
5. _git --version_ -If git is already installed, his will show the version of the git.

